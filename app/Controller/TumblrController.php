<?php

class TumblrController extends AppController {
	public $helpers = array('Html', 'Form');


	public function index () {
		$title = 'View Tumblr Blogs';
		$this->set('title_for_layout', $title);

		$this->set("blogName", "");
		$this->set("error", "");
		$this->set("blogPosts", "");
		$this->set('pagingDisplay', "");


		if($this->request->is('post')) {
			//print_r($this->request->data);
			$blogInfo = $this->Tumblr->CheckBlog($this->request->data);

			if(empty($blogInfo['error'])) {
				$blogPosts = $this->Tumblr->GetPosts($this->request->data);

				$this->set("blogPosts", $blogPosts);

				$this->set("blogName", $this->request->data['blog_post_form']['blogName']);

				$pagingData = $this->Tumblr->CustomPaging($this->request->data);
				$this->set('pagingDisplay', $pagingData);
			}
			else
			{
				$this->set("error", "Tumblr Blog Not Found.  Please try a different Blog Name.");
			}
		}
	}

	public function getPostsAjax(){
		//$this->layout = "ajax";
		$this->layout=null;

		if($this->request->is('post')) {
			//print_r($this->request->data);
			$ajaxVars = array();
			$ajaxVars['blog_post_form']['current_page'] = $this->request->data['pageNum'];
			$ajaxVars['blog_post_form']['blogName'] = $this->request->data['blogName'];
			$ajaxVars['blog_post_form']['sentFrom'] = $this->request->data['sentFrom'];

			$ajaxOutput= array();

			$blogInfo = $this->Tumblr->CheckBlog($ajaxVars);

			if(empty($blogInfo['error'])) {
				$blogPosts = $this->Tumblr->GetPosts($ajaxVars);

				$ajaxOutput['blogPosts'] ="<ul id='post_list'>";
		
			
				foreach($blogPosts as $post) {
					$ajaxOutput['blogPosts'] .="<li class='post_row'><span class='post_number'>".$post['post_number'].".</span> ID: <a href='".$post['post_url']."' target='_blank'>".$post['id']."</a> - Posted On: ".$post['date']." </li>";
				}
		
				$ajaxOutput['blogPosts'] .="</ul>";


				$pagingData = $this->Tumblr->CustomPaging($ajaxVars);

				$ajaxOutput['pagingOutput'] = $pagingData;
				$ajaxOutput['error'] = "";

			}
			else
			{
				
				$ajaxOutput['error'] = "Tumblr Blog Not Found.  Please try a different Blog Name.";
			}
			
			echo json_encode($ajaxOutput);
			exit();
		}
	}
}