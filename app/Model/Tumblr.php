<?php

class Tumblr extends AppModel {

	private $apikey = "fuiKNFp9vQFvjLNvx4sUwti4Yb5yGutBN4Xh10LXZhhRKjWlV4";
	public $totalPosts;
	public $limit = 10;

	public function CheckBlog($data){

		$tumblr = $data['blog_post_form']['blogName'].'.tumblr.com';
		
		$blogInfoArray = array("totalPosts"=>"", "error"=>"");

		
		if($apiInfo = json_decode(file_get_contents("http://api.tumblr.com/v2/blog/$tumblr/info?api_key=".$this->apikey))) {

	    	$blogInfoData = $apiInfo->response->blog;
	    	$blogInfoArray['totalPosts'] = $apiInfo->response->blog->posts;
	    	$this->totalPosts = $apiInfo->response->blog->posts;
	    }
	    else {

	    	$blogInfoArray['error'] = "Yes";
	    }


    	return $blogInfoArray;
	}


	public function GetPosts($data){
		$tz = new DateTimeZone('America/New_York');
	    
	    
	    if((isset($data['blog_post_form']['current_page']) && !empty($data['blog_post_form']['current_page'])) && (isset($data['blog_post_form']['sentFrom']) && !empty($data['blog_post_form']['sentFrom']))){
	    	$offset = ($data['blog_post_form']['current_page'] - 1) * $this->limit;
	    }
	    else{
	    	$offset = 0;
		}

		if($offset >= $limit){
			$cn = $offset + 1;
		}
		else {
			$cn=1;
		}

	    $tumblr = $data['blog_post_form']['blogName'].'.tumblr.com';


	    $apidata = json_decode(file_get_contents("http://api.tumblr.com/v2/blog/$tumblr/posts?api_key=".$this->apikey."&limit=".$this->limit."&offset=$offset"));

	    $mypostsdata = $apidata->response->posts;
	    $myposts = array();

	    $i = 0;
	    foreach($mypostsdata as $postdata) {
	        $post['post_number'] = $cn;
	        $post['id'] = $postdata->id;

	        //$cdate = strtotime($postdata->date);
	        $pdate = new DateTime($postdata->date);
	        $pdate->setTimeZone($tz);
	        $date_output = $pdate->format('l F jS, Y g:i:s A')." EST";
	        $post['date'] = $date_output;
	        $post['post_url'] = $postdata->post_url;

	        $myposts[$i] = $post;
	        $i++;
	        $cn++;
	    }

	    return $myposts;
	
	}

	public function CustomPaging($data) {
		
		$numPages = ceil($this->totalPosts / $this->limit);

		if($numPages < 10) {
			$displayMax = $numPages;
		}
		else{
			$displayMax = 10;
		}

		if(!isset($data['blog_post_form']['sentFrom']) ) {
			$data['blog_post_form']['current_page'] = 1;
		}

		$pagingOutput = "<div id='paging_controls'>";

		if($data['blog_post_form']['current_page'] > 1){
			$pagingOutput .="<a href='#' class='paging_display' onclick='changePage(1);'>First</a> &nbsp;&nbsp;";
			$pagingOutput .="<a href='#' class='paging_display' onclick='changePage(".($data['blog_post_form']['current_page']-1).");'>Previous</a> &nbsp;&nbsp;";
		}
		else
		{
			$pagingOutput .="Previous &nbsp;&nbsp;";
		}

		$i = 0;

		if($data['blog_post_form']['current_page'] <= 5){
			$n = 1;
		}
		elseif($numPages - $data['blog_post_form']['current_page'] < 10) {
			$n= $numPages - 9;
		}
		else {
			$n = $data['blog_post_form']['current_page'] - 5;
		}

		while($i < $displayMax) {
			if($data['blog_post_form']['current_page'] != $n) {
				$pagingOutput .="<a href='#' class='paging_display' onclick='changePage(".$n.");'>".$n."</a> &nbsp;&nbsp;";
			}
			else {
				$pagingOutput .="<span class='current_page'>".$n."</span> &nbsp;&nbsp;";
			}
			
			$n++;
			$i++;
		}

		if($data['blog_post_form']['current_page'] < $numPages){
			$pagingOutput .="<a href='#' class='paging_display' onclick='changePage(".($data['blog_post_form']['current_page']+1).");'>Next</a> &nbsp;&nbsp;";
			$pagingOutput .="<a href='#' class='paging_display' onclick='changePage(".$numPages.");'>Last</a> &nbsp;&nbsp;";
		}
		else
		{
			$pagingOutput .="Next &nbsp;&nbsp;";
		}

		$pagingOutput .="</div>";

		$pagingOutput .="<div id='total_display'>Total Posts: ".$this->totalPosts."</div>";

		return $pagingOutput;

	}
	
}