<?php 
	$this->Html->css('styles', null, array('inline' => false));
	$this->Html->script('tumblr_posts', array('inline'=>false));
?>

<h1>Get Tumblr Blog Posts <?php if(isset($blogName) && !empty($blogName)) {echo "from $blogName";} ?></h1>

<?php
	echo $this->Form->create('blog_post_form', array('type'=>'post'));
	echo $this->Form->input('blogName', array('value'=>$blogName, 'label'=>array('text'=>'.tumblr.com', 'class'=>'blog_input_label'), 'format' => array('before', 'input', 'between', 'label', 'after', 'error'), 'class'=>'blog_input'));
	echo $this->Form->input('current_page', array('type'=>'hidden', 'value'=>'1'));
	echo $this->Form->end('Get Blog Posts');

	if(!empty($error))
	{
		echo "<p class='error_msg'>$error</p>";
	}

?>

<div id='data_loading' align='center' style='width:150px;display:none;'><img src='img/ajax-loader.gif' height='32' width='32'></div>
<div id='post_data_container'>
	<?php if(!empty($blogPosts)): ?>
	<ul id='post_list'>
		<?php 
			
			foreach($blogPosts as $post) {
				echo "<li class='post_row'><span class='post_number'>".$post['post_number'].".</span> ID: <a href='".$post['post_url']."' target='_blank'>".$post['id']."</a> - Posted On: ".$post['date']." </li>";
			}
		?>
	</ul>
	<?php ENDIF; ?>


</div>

<div id='pageination_container'>
	<?php 
		echo $pagingDisplay;
	?>
</div>