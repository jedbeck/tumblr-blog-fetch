

function changePage(pageNum) {
		//alert("Page Num " + pageNum );
		$('#post_data_container').hide();
		$('#pageination_container').hide();
		$('#data_loading').show();
		$.ajax({
			type: "post",
			url: 'tumblr/getPostsAjax',
			cache: false,
			dataType: 'json',
			data : {
				pageNum: pageNum, 
				blogName: $('#blog_post_formBlogName').val(),
				sentFrom: 'ajax'
			},
			success:function (data){
				$("#blog_post_formCurrentPage").val(pageNum);
				$('#post_data_container').html(data.blogPosts);
				$('#pageination_container').html(data.pagingOutput);
				
				$('#data_loading').hide();
				$('#post_data_container').show();
				$('#pageination_container').show();
				
			}
		})
	}
