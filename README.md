# Tumblr Blog Fetch

Live example of this code is available at http://tumblr.jedbeck.com/

This code example was achieves the following objectives.

Build a form that accepts a Tumblr blog name as it’s input and use the Tumblr API to retrieve posts for that blog.

Results should be shown 10 at a time and have correctly functioning pagination links. ie. ‘next’ link only works if there are more posts, ‘3’ link should show results 21-30, etc.

For each post, show it’s post ID, publish date and a link to the post on Tumblr.

Use of a PHP framework (CakePHP is used in this example)

An emphasis should not be given on design. However, the post results should still be readable